extern crate reqwest;
extern crate toml;
extern crate serde_json;

use self::reqwest::{Client, Response};
use self::toml::Value;
use self::serde_json::Value as JsonValue;
use self::serde_json::from_str;
use std::io::copy;
use std::io::prelude::*;
use std::fs::File;

static LTP_BASE : &'static str = "https://www3.elearning.rwth-aachen.de/_vti_bin/l2pservices/api.svc/v1/";

fn get_access_token() -> String {
    let mut config_string = String::new();
    let mut config_file = File::open(".rwth.toml").expect("Something went wrong opening .rwth.toml");
    config_file.read_to_string(&mut config_string)
        .expect("something went wrong reading the file");
    let config_data = config_string.parse::<Value>().unwrap();

    config_data["access_data"]["access_token"].as_str().unwrap().to_owned()
}

fn ltp_get_request(method: &str, params: &[(&str, &str)]) -> Response {
    let response;
    let client = Client::new();
    let mut request_url = LTP_BASE.to_owned()
                    + method
                    + "?accessToken=" 
                    + &get_access_token();

    for param in params {
        request_url.push_str(&"&");
        request_url.push_str(param.0);
        request_url.push_str("=");
        request_url.push_str(param.1);
    }

    response = client.get(&request_url)
        .send()
        .unwrap();

    response
}

#[allow(unused)]
pub fn view_user_info() {
    let response = ltp_get_request("viewUserInfo", &[]).text().unwrap();
}

pub fn ping() {
     let mut response = ltp_get_request("Ping", &[("p", "Pong")]);

    if response.status().is_success() {
        println!("Pong");
    } else {
        println!("{}", response.text().unwrap());
    }
}
#[allow(unused)]
pub fn view_all_course_events() -> JsonValue {
    let mut response = ltp_get_request("viewAllCourseEvents", &[]);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();
        
        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn view_all_course_info() -> JsonValue {
    let mut response = ltp_get_request("viewAllCourseInfo", &[]);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();
        
        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn view_all_learning_materials(cid: &str) -> JsonValue {
    let params = [
        ("cid", cid)
    ];

    let mut response = ltp_get_request("viewAllLearningMaterials", &params);

    if response.status().is_success() {
        let data: JsonValue = from_str(&response.text().unwrap()).unwrap();
        
        data
    } else {
        panic!("{}", &response.text().unwrap());
    }
}

pub fn download_file(file_name: &str, cid: &str, download_url: &str, path: &str) {
    let params = [
        ("cid", cid),
        ("downloadUrl", download_url)
    ];

    let mut response = ltp_get_request(&("downloadFile/".to_owned() + file_name), &params);

    let mut file = File::create(path.to_owned() + "/" + file_name).unwrap();
    copy(&mut response, &mut file).unwrap();
}
