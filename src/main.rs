mod api;
mod user_interaction;
mod config_access;

extern crate toml;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate clap;
#[macro_use]
extern crate serde_derive;

use api::ltp::*;
use user_interaction::clap_subcommands::*;
use config_access::*;

use clap::{App, SubCommand};

fn main() {
    let mut config_data = toml_from_config();
    check_and_refresh_config(&mut config_data);

    let matches = App::new(crate_name!())
            .about("A command line tool to synchronize your learning materials with your local files.")
            .version(crate_version!())
            .subcommand(SubCommand::with_name("courses")
                                        .about("Lists your courses"))
            .subcommand(SubCommand::with_name("ping")
                                        .about("Pings the L2P"))
            .subcommand(SubCommand::with_name("sync")
                                        .about("Downloads all available files from your L2P and creates an according folder structure"))
            .get_matches();

    if let Some(_) = matches.subcommand_matches("courses") {
        list_courses(view_all_course_info());
    }

    if let Some(_) = matches.subcommand_matches("ping") {
        ping();
    }

    if let Some(_) = matches.subcommand_matches("sync") {
        sync_all(view_all_course_info());
    }
}
