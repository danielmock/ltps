extern crate ansi_term;
extern crate serde_json;

use self::serde_json::Value as JsonValue;
use self::ansi_term::{Color, Style};
use std::fs::create_dir_all;
use ::api::ltp::*;

pub fn list_courses(courses_json: JsonValue) {
    let courses = courses_json["dataSet"].as_array().unwrap();
    let semesters: Vec<String> = get_semesters(courses);

    //sort_for_semesters(&semesters);

    for semester in semesters {
        println!("{}:", Style::new().bold().paint(&semester));
        for course in courses {
            if course["semester"].as_str().unwrap() == &semester {
                println!("\t{} - {}:\n\t{}", 
                            course["courseTitle"].as_str().unwrap(),
                            course["description"].as_str().unwrap(),
                            Color::Blue.paint(course["url"].as_str().unwrap()).to_string());
            }
        }
    }
}

pub fn sync_all(courses_json: JsonValue) {
    let courses = courses_json["dataSet"].as_array().unwrap();
    let semesters: Vec<String> = get_semesters(courses);
    let mut learning_materials: JsonValue;
    let mut path;

    for semester in semesters {
        for course in courses {
            path = "".to_string();
            path.push_str(&semester);
            if course["semester"].as_str().unwrap() == &semester && &semester == "ss18" {
                path.push_str("/");
                path.push_str(course["courseTitle"].as_str().unwrap());
                create_dir_all(&path).unwrap();

                learning_materials = view_all_learning_materials(course["uniqueid"].as_str().unwrap());
                
                for material in learning_materials["dataSet"].as_array().unwrap() {
                    if !material["isDirectory"].as_bool().unwrap() &&
                        material["fileInformation"]["fileName"].as_str().unwrap().contains(".pdf") 
                        {
                            download_file(material["fileInformation"]["fileName"].as_str().unwrap(),
                                            course["uniqueid"].as_str().unwrap(),
                                            material["fileInformation"]["downloadUrl"].as_str().unwrap(),
                                            &path);
                    }
                }
            }
        }
    }
}

fn get_semesters(courses_array: &Vec<JsonValue>) -> Vec<String> {
    let mut semesters: Vec<String> = Vec::new();

    for course in courses_array {
        if !semesters.contains(&course["semester"].as_str().unwrap().to_string()) {
            semesters.push(course["semester"].as_str().unwrap().to_string());
        }
    }

    semesters
}

#[allow(unused)]
fn sort_for_semesters(semesters: &Vec<String>) {
    //TODO
}
